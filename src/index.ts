import { PrismaClient } from "@prisma/client";
import express from "express";
import env from "env-var";
import cookieParser from "cookie-parser";
import cors from "cors";
import { Server } from "socket.io";
import http from "http";
import auth from "./auth";
import servers from "./server";

const app = express();
const server = http.createServer(app);
const io = new Server(server, {
	cors: {
		origin: env.get("CLIENT_URL").asString()!,
	},
	cookie: true,
});
const db = new PrismaClient();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cookieParser());
app.use(
	cors({
		allowedHeaders: ["content-type"],
		credentials: true,
		origin: env.get("CLIENT_URL").asString()!,
	})
);
app.use("/auth", auth);
app.use("/servers", servers);

app.get("/", (req, res) => {
	res.json({ msg: "hello world", count: 1, admin: false });
});

io.on("connection", async (socket) => {
	// console.log(socket.handshake.query);
	const messages = await db.message.findMany({
		where: {
			channelId: socket.handshake.query.channelId as string,
		},
	});

	socket.broadcast
		.to(socket.handshake.query.channelId as string)
		.emit("all_messages", messages);

	socket.on("send_message", (data) => {
		console.log(socket.id);
		console.log(data);

		socket.broadcast.emit("receive_message", data);
	});
});

server.listen(4000, () => {
	console.log("listening on port 4000");
});
