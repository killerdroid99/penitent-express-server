import { PrismaClient } from "@prisma/client";
import express from "express";
import bcrypt from "bcrypt";
import env from "env-var";
import jwt from "jsonwebtoken";
import z from "zod";
import axios from "axios";

const auth = express();
const db = new PrismaClient();

auth.get("/login/discord", (req, res) => {
	const url =
		"https://discord.com/oauth2/authorize?client_id=1248838981261459498&response_type=code&redirect_uri=http%3A%2F%2Flocalhost%3A4000%2Fauth%2Flogin%2Fdiscord%2Fredirect&scope=email+identify";

	res.redirect(url);
});

auth.post("/login", async (req, res) => {
	const loginSchema = z.object({
		email: z.string().trim().email(),
		password: z
			.string()
			.trim()
			.min(3, "Password must contain at least 3 characters")
			.max(20, "Password must not exceed 20 characters"),
	});

	const { success, error, data } = await loginSchema.safeParseAsync(req.body);

	if (!success) {
		return res.status(400).send({
			email: error.format().email?._errors[0],
			password: error.format().password?._errors[0],
		});
	}

	const user = await db.user.findUnique({ where: { email: data.email } });

	if (user === null) {
		return res.status(404).send({
			email: "User with this email not found",
		});
	}

	if (user.creationMethod !== "local") {
		return res.status(400).send({
			msg: "Login with discord",
		});
	}

	if (!(await bcrypt.compare(data.password, user.password!))) {
		return res.status(403).send({
			password: "Incorrect password",
		});
	}

	const token = await jwt.sign(
		{
			userId: user.id,
		},
		env.get("JWT_SECRET").asString()!
	);

	res.cookie(env.get("JWT_COOKIE_NAME").asString()!, token, {
		path: "/",
		maxAge: 1000 * 60 * 60 * 24,
		httpOnly: false,
		secure: false,
		sameSite: "lax",
	});

	return res.status(200).json({
		msg: "Logged in",
	});
});

auth.post("/register", async (req, res) => {
	const registerSchema = z.object({
		name: z
			.string()
			.trim()
			.min(2, "Name must contain at least 2 characters")
			.max(12, "Name must not exceed 12 characters"),
		email: z.string().trim().email(),
		password: z
			.string()
			.trim()
			.min(3, "Password must contain at least 3 characters")
			.max(20, "Password must not exceed 20 characters"),
	});

	const { success, error, data } = await registerSchema.safeParseAsync(
		req.body
	);

	if (!success) {
		return res.status(400).send({
			name: error.format().name?._errors[0],
			email: error.format().email?._errors[0],
			password: error.format().password?._errors[0],
		});
	}

	const u = await db.user.findUnique({
		where: {
			email: data.email,
		},
	});

	if (u !== null) {
		return res.status(400).send({
			email: "User with this email already exists",
		});
	}

	const salt = await bcrypt.genSalt(10);
	const user = await db.user.create({
		data: {
			...data,
			creationMethod: "local",
			password: await bcrypt.hash(data.password, salt),
		},
	});

	const token = jwt.sign(
		{
			userId: user.id,
		},
		env.get("JWT_SECRET").asString()!
	);

	res.cookie(env.get("JWT_COOKIE_NAME").asString()!, token, {
		path: "/",
		maxAge: 1000 * 60 * 60 * 24,
		httpOnly: false,
		secure: false,
		sameSite: "lax",
	});

	return res.status(201).json({
		msg: "Registered and logged in",
	});
});

auth.get("/me", async (req, res) => {
	const token = req.cookies[env.get("JWT_COOKIE_NAME").asString()!];

	if (token === undefined) {
		return res.status(401).send({
			msg: "No token found",
		});
	}

	if (!jwt.verify(token, env.get("JWT_SECRET").asString()!)) {
		return res.status(401).send({
			msg: "Invalid or expired token",
		});
	}

	const data = jwt.decode(token) as { userId: string };

	const user = await db.user.findUnique({ where: { id: data.userId } });
	return res.json({
		name: user?.name,
		creationMethod: user?.creationMethod,
		avatar: user?.avatar,
	});
});

auth.post("/logout", async (req, res) => {
	const token = req.cookies[env.get("JWT_COOKIE_NAME").asString()!];

	if (token === undefined) {
		return res.status(400).send({
			msg: "No token found",
		});
	}

	res.clearCookie(env.get("JWT_COOKIE_NAME").asString()!);
	return res.sendStatus(200);
});

auth.get("/login/discord/redirect", async (req, res) => {
	const { code } = req.query;

	const params = new URLSearchParams({
		client_id: env.get("DISCORD_CLIENT_ID").asString()!,
		client_secret: env.get("DISCORD_CLIENT_SECRET").asString()!,
		grant_type: "authorization_code",
		code: code as string,
		redirect_uri: env.get("DISCORD_REDIRECT_URI").asString()!,
	});

	const headers = {
		"content-type": "application/x-www-form-urlencoded",
		"accept-encoding": "application/x-www-form-urlencoded",
	};

	const response = await axios.post(
		"https://discord.com/api/oauth2/token",
		params,
		{ headers }
	);

	const userResponse = await axios.get("https://discord.com/api/users/@me", {
		headers: {
			Authorization: `Bearer ${response.data.access_token}`,
		},
	});

	const user = await db.user.findUnique({
		where: {
			email: userResponse.data.email,
		},
	});

	if (user === null) {
		const newUser = await db.user.create({
			data: {
				creationMethod: "discord",
				email: userResponse.data.email,
				name: userResponse.data.username,
				avatar: `https://cdn.discordapp.com/avatars/${userResponse.data.id}/${userResponse.data.avatar}.webp`,
			},
		});

		const token = jwt.sign(
			{
				userId: newUser.id,
			},
			env.get("JWT_SECRET").asString()!
		);

		res.cookie(env.get("JWT_COOKIE_NAME").asString()!, token, {
			path: "/",
			maxAge: 1000 * 60 * 60 * 24,
			httpOnly: false,
			secure: false,
			sameSite: "lax",
		});

		return res
			.status(201)
			.json({
				msg: "Logged in with discord",
			})
			.redirect(env.get("CLIENT_URL").asString()!);
	}

	const token = jwt.sign(
		{
			userId: user.id,
		},
		env.get("JWT_SECRET").asString()!
	);

	res.cookie(env.get("JWT_COOKIE_NAME").asString()!, token, {
		path: "/",
		maxAge: 1000 * 60 * 60 * 24,
		httpOnly: false,
		secure: false,
		sameSite: "lax",
	});

	return res.redirect("http://localhost:3000/");
});

auth.post("/login", async (req, res) => {
	const loginSchema = z.object({
		email: z.string().trim().email(),
		password: z
			.string()
			.trim()
			.min(3, "Password must contain at least 3 characters")
			.max(20, "Password must not exceed 20 characters"),
	});

	console.log(req.body);
	const { success, error, data } = await loginSchema.safeParseAsync(req.body);

	if (!success) {
		return res.status(400).send({
			email: error.format().email?._errors[0],
			password: error.format().password?._errors[0],
		});
	}

	const user = await db.user.findUnique({ where: { email: data.email } });

	if (user === null) {
		return res.status(404).send({
			email: "User with this email not found",
		});
	}

	if (user.creationMethod !== "local") {
		return res.status(400).send({
			msg: "Login with discord",
		});
	}

	if (!(await bcrypt.compare(data.password, user.password!))) {
		return res.status(403).send({
			password: "Incorrect password",
		});
	}

	const token = await jwt.sign(
		{
			userId: user.id,
		},
		env.get("JWT_SECRET").asString()!
	);

	res.cookie(env.get("JWT_COOKIE_NAME").asString()!, token, {
		path: "/",
		maxAge: 1000 * 60 * 60 * 24,
		httpOnly: false,
		secure: false,
		sameSite: "lax",
	});

	return res.status(200).json({
		msg: "Logged in",
	});
});

auth.post("/register", async (req, res) => {
	const registerSchema = z.object({
		name: z
			.string()
			.trim()
			.min(2, "Name must contain at least 2 characters")
			.max(12, "Name must not exceed 12 characters"),
		email: z.string().trim().email(),
		password: z
			.string()
			.trim()
			.min(3, "Password must contain at least 3 characters")
			.max(20, "Password must not exceed 20 characters"),
	});

	const { success, error, data } = await registerSchema.safeParseAsync(
		req.body
	);

	if (!success) {
		return res.status(400).send({
			name: error.format().name?._errors[0],
			email: error.format().email?._errors[0],
			password: error.format().password?._errors[0],
		});
	}

	const u = await db.user.findUnique({
		where: {
			email: data.email,
		},
	});

	if (u !== null) {
		return res.status(400).send({
			email: "User with this email already exists",
		});
	}

	const salt = await bcrypt.genSalt(10);
	const user = await db.user.create({
		data: {
			...data,
			creationMethod: "local",
			password: await bcrypt.hash(data.password, salt),
		},
	});

	const token = jwt.sign(
		{
			userId: user.id,
		},
		env.get("JWT_SECRET").asString()!
	);

	res.cookie(env.get("JWT_COOKIE_NAME").asString()!, token, {
		path: "/",
		maxAge: 1000 * 60 * 60 * 24,
		httpOnly: false,
		secure: false,
		sameSite: "lax",
	});

	return res.status(201).json({
		msg: "Registered and logged in",
	});
});

auth.get("/me", async (req, res) => {
	const token = req.cookies[env.get("JWT_COOKIE_NAME").asString()!];

	if (token === undefined) {
		return res.status(401).send({
			msg: "No token found",
		});
	}

	if (!jwt.verify(token, env.get("JWT_SECRET").asString()!)) {
		return res.status(401).send({
			msg: "Invalid or expired token",
		});
	}

	const data = jwt.decode(token) as { userId: string };

	const user = await db.user.findUnique({ where: { id: data.userId } });
	return res.json({
		name: user?.name,
		creationMethod: user?.creationMethod,
		avatar: user?.avatar,
	});
});

auth.post("/logout", async (req, res) => {
	const token = req.cookies[env.get("JWT_COOKIE_NAME").asString()!];

	if (token === undefined) {
		return res.status(400).send({
			msg: "No token found",
		});
	}

	res.clearCookie(env.get("JWT_COOKIE_NAME").asString()!);
	return res.sendStatus(200);
});

export default auth;
