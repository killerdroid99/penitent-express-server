import { PrismaClient } from "@prisma/client";
import express from "express";
import env from "env-var";
import jwt from "jsonwebtoken";
import z from "zod";

const servers = express.Router();
const db = new PrismaClient();

servers.post("/create-server", async (req, res) => {
	const token = req.cookies[env.get("JWT_COOKIE_NAME").asString()!];

	if (token === undefined) {
		return res.status(401).send({
			msg: "No token found",
		});
	}

	if (!jwt.verify(token, env.get("JWT_SECRET").asString()!)) {
		return res.status(401).send({
			msg: "Invalid or expired token",
		});
	}

	const userData = jwt.decode(token) as { userId: string };

	const createServerSchema = z.object({
		serverName: z
			.string()
			.trim()
			.min(5, "Must contain at least 5 characters")
			.max(30, "Must not exceed 30 characters"),
		mainChannelName: z
			.string()
			.trim()
			.min(5, "Must contain at least 3 characters")
			.max(30, "Must not exceed 50 characters"),
	});

	const { success, error, data } = await createServerSchema.safeParseAsync(
		req.body
	);

	if (!success) {
		return res.status(400).send({
			serverName: error.format().serverName?._errors[0],
			mainChannelName: error.format().mainChannelName?._errors[0],
		});
	}

	const roleEnum = z.enum(["ADMIN", "REGULAR"]);

	await db.server.create({
		data: {
			name: data.serverName,
			members: {
				create: {
					role: roleEnum.Enum.ADMIN,
					memberId: userData.userId,
				},
			},
			channels: {
				create: {
					topic: data.mainChannelName,
				},
			},
		},
	});

	return res.status(200).json({
		msg: "Server created",
	});
});

servers.get("/joined-servers", async (req, res) => {
	const token = req.cookies[env.get("JWT_COOKIE_NAME").asString()!];

	if (token === undefined) {
		return res.status(401).send({
			msg: "No token found",
		});
	}

	if (!jwt.verify(token, env.get("JWT_SECRET").asString()!)) {
		return res.status(401).send({
			msg: "Invalid or expired token",
		});
	}

	const userData = jwt.decode(token) as { userId: string };

	const servers = await db.userToChannel.findMany({
		where: {
			memberId: userData.userId,
		},
		select: {
			server: true,
		},
	});

	return res.status(200).send({ servers });
});

servers.get("/server/:id", async (req, res) => {
	const token = req.cookies[env.get("JWT_COOKIE_NAME").asString()!];

	if (token === undefined) {
		return res.status(401).send({
			msg: "No token found",
		});
	}

	if (!jwt.verify(token, env.get("JWT_SECRET").asString()!)) {
		return res.status(401).send({
			msg: "Invalid or expired token",
		});
	}

	const serverData = await db.server.findUnique({
		where: {
			id: req.params.id,
		},
		include: {
			channels: {
				select: {
					id: true,
					topic: true,
				},
			},
		},
	});

	if (serverData === null) {
		return res.status(404).send({
			msg: "Server not found",
		});
	}

	return res.status(200).send(serverData);
});

export default servers;
