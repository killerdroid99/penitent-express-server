FROM node:22.3.0-alpine3.19

ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"

RUN corepack enable
# RUN pnpm prisma db push

WORKDIR /usr/app
COPY . .
RUN pnpm install
EXPOSE 4000
RUN pnpm prisma db push
CMD [ "pnpm", "start" ]
